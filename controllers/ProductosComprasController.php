<?php

namespace app\controllers;

use app\models\ProductosCompras;
use app\models\ProductosComprasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductosComprasController implements the CRUD actions for ProductosCompras model.
 */
class ProductosComprasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all ProductosCompras models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ProductosComprasSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProductosCompras model.
     * @param int $IDproductoCompra I Dproducto Compra
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($IDproductoCompra)
    {
        return $this->render('view', [
            'model' => $this->findModel($IDproductoCompra),
        ]);
    }

    /**
     * Creates a new ProductosCompras model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new ProductosCompras();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'IDproductoCompra' => $model->IDproductoCompra]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ProductosCompras model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $IDproductoCompra I Dproducto Compra
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($IDproductoCompra)
    {
        $model = $this->findModel($IDproductoCompra);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'IDproductoCompra' => $model->IDproductoCompra]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ProductosCompras model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $IDproductoCompra I Dproducto Compra
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($IDproductoCompra)
    {
        $this->findModel($IDproductoCompra)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProductosCompras model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $IDproductoCompra I Dproducto Compra
     * @return ProductosCompras the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($IDproductoCompra)
    {
        if (($model = ProductosCompras::findOne(['IDproductoCompra' => $IDproductoCompra])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
