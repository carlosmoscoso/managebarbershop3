<?php

namespace app\controllers;

use app\models\ProductosVentas;
use app\models\ProductosVentasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductosVentasController implements the CRUD actions for ProductosVentas model.
 */
class ProductosVentasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all ProductosVentas models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ProductosVentasSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProductosVentas model.
     * @param int $IDproductoVenta I Dproducto Venta
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($IDproductoVenta)
    {
        return $this->render('view', [
            'model' => $this->findModel($IDproductoVenta),
        ]);
    }

    /**
     * Creates a new ProductosVentas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new ProductosVentas();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'IDproductoVenta' => $model->IDproductoVenta]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ProductosVentas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $IDproductoVenta I Dproducto Venta
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($IDproductoVenta)
    {
        $model = $this->findModel($IDproductoVenta);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'IDproductoVenta' => $model->IDproductoVenta]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ProductosVentas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $IDproductoVenta I Dproducto Venta
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($IDproductoVenta)
    {
        $this->findModel($IDproductoVenta)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProductosVentas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $IDproductoVenta I Dproducto Venta
     * @return ProductosVentas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($IDproductoVenta)
    {
        if (($model = ProductosVentas::findOne(['IDproductoVenta' => $IDproductoVenta])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
