<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TelefonosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="telefonos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'IDtelefono') ?>

    <?= $form->field($model, 'telefono1') ?>

    <?= $form->field($model, 'telefono2') ?>

    <?= $form->field($model, 'IDtelefono_cliente') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
