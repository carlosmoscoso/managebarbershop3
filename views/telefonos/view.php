<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Telefonos */

$this->title = $model->IDtelefono;
$this->params['breadcrumbs'][] = ['label' => 'Telefonos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="telefonos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'IDtelefono' => $model->IDtelefono], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'IDtelefono' => $model->IDtelefono], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'IDtelefono',
            'telefono1',
            'telefono2',
            'IDtelefono_cliente',
        ],
    ]) ?>

</div>
