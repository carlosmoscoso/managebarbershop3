<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosVentasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productos-ventas-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'IDproductoVenta') ?>

    <?= $form->field($model, 'fecha') ?>

    <?= $form->field($model, 'IDproducto_venta_cliente') ?>

    <?= $form->field($model, 'IDproducto_venta_producto') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
