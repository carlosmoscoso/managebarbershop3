<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosVentas */

$this->title = $model->IDproductoVenta;
$this->params['breadcrumbs'][] = ['label' => 'Productos Ventas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="productos-ventas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'IDproductoVenta' => $model->IDproductoVenta], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'IDproductoVenta' => $model->IDproductoVenta], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'IDproductoVenta',
            'fecha',
            'IDproducto_venta_cliente',
            'IDproducto_venta_producto',
        ],
    ]) ?>

</div>
