<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductosComprasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Productos Compras';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productos-compras-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Productos Compras', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'IDproductoCompra',
            'fecha',
            'IDproducto_compra_proveedor',
            'IDproducto_compra_producto',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, ProductosCompras $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'IDproductoCompra' => $model->IDproductoCompra]);
                 }
            ],
        ],
    ]); ?>


</div>
