<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosComprasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productos-compras-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'IDproductoCompra') ?>

    <?= $form->field($model, 'fecha') ?>

    <?= $form->field($model, 'IDproducto_compra_proveedor') ?>

    <?= $form->field($model, 'IDproducto_compra_producto') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
