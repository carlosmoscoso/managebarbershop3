<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosCompras */

$this->title = $model->IDproductoCompra;
$this->params['breadcrumbs'][] = ['label' => 'Productos Compras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="productos-compras-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'IDproductoCompra' => $model->IDproductoCompra], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'IDproductoCompra' => $model->IDproductoCompra], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'IDproductoCompra',
            'fecha',
            'IDproducto_compra_proveedor',
            'IDproducto_compra_producto',
        ],
    ]) ?>

</div>
