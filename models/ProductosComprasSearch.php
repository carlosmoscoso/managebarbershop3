<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProductosCompras;

/**
 * ProductosComprasSearch represents the model behind the search form of `app\models\ProductosCompras`.
 */
class ProductosComprasSearch extends ProductosCompras
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IDproductoCompra', 'IDproducto_compra_proveedor', 'IDproducto_compra_producto'], 'integer'],
            [['fecha'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductosCompras::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'IDproductoCompra' => $this->IDproductoCompra,
            'fecha' => $this->fecha,
            'IDproducto_compra_proveedor' => $this->IDproducto_compra_proveedor,
            'IDproducto_compra_producto' => $this->IDproducto_compra_producto,
        ]);

        return $dataProvider;
    }
}
