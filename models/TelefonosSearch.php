<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Telefonos;

/**
 * TelefonosSearch represents the model behind the search form of `app\models\Telefonos`.
 */
class TelefonosSearch extends Telefonos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IDtelefono', 'IDtelefono_cliente'], 'integer'],
            [['telefono1', 'telefono2'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Telefonos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'IDtelefono' => $this->IDtelefono,
            'IDtelefono_cliente' => $this->IDtelefono_cliente,
        ]);

        $query->andFilterWhere(['like', 'telefono1', $this->telefono1])
            ->andFilterWhere(['like', 'telefono2', $this->telefono2]);

        return $dataProvider;
    }
}
