<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property int $IDproducto
 * @property string $nombre
 * @property float $costeProveedores
 * @property float $costeCliente
 *
 * @property ProductosCompras[] $productosCompras
 * @property ProductosVentas[] $productosVentas
 */
class Productos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'costeProveedores', 'costeCliente'], 'required'],
            [['costeProveedores', 'costeCliente'], 'number'],
            [['nombre'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IDproducto' => 'I Dproducto',
            'nombre' => 'Nombre',
            'costeProveedores' => 'Coste Proveedores',
            'costeCliente' => 'Coste Cliente',
        ];
    }

    /**
     * Gets query for [[ProductosCompras]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosCompras()
    {
        return $this->hasMany(ProductosCompras::className(), ['IDproducto_compra_producto' => 'IDproducto']);
    }

    /**
     * Gets query for [[ProductosVentas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosVentas()
    {
        return $this->hasMany(ProductosVentas::className(), ['IDproducto_venta_producto' => 'IDproducto']);
    }
}
