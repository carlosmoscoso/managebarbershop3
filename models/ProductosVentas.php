<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos_ventas".
 *
 * @property int $IDproductoVenta
 * @property string $fecha
 * @property int|null $IDproducto_venta_cliente
 * @property int|null $IDproducto_venta_producto
 *
 * @property Clientes $iDproductoVentaCliente
 * @property Productos $iDproductoVentaProducto
 */
class ProductosVentas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos_ventas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'required'],
            [['fecha'], 'safe'],
            [['IDproducto_venta_cliente', 'IDproducto_venta_producto'], 'integer'],
            [['IDproducto_venta_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['IDproducto_venta_cliente' => 'IDcliente']],
            [['IDproducto_venta_producto'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['IDproducto_venta_producto' => 'IDproducto']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IDproductoVenta' => 'I Dproducto Venta',
            'fecha' => 'Fecha',
            'IDproducto_venta_cliente' => 'I Dproducto Venta Cliente',
            'IDproducto_venta_producto' => 'I Dproducto Venta Producto',
        ];
    }

    /**
     * Gets query for [[IDproductoVentaCliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIDproductoVentaCliente()
    {
        return $this->hasOne(Clientes::className(), ['IDcliente' => 'IDproducto_venta_cliente']);
    }

    /**
     * Gets query for [[IDproductoVentaProducto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIDproductoVentaProducto()
    {
        return $this->hasOne(Productos::className(), ['IDproducto' => 'IDproducto_venta_producto']);
    }
}
